<?php

if ( ! class_exists('EmailHandler') ) {
    class EmailHandler
    {
        private static $instance;

        private $headers = [ 'Content-Type: text/html; charset=UTF-8', 'From: Gourmet Basket <admin@gourmetbasket.com.au>' ];
        private $to = [ "jax@blaze.online", "dev@blaze.online", "stuart@gourmetbasket.com.au" ];
        public $subject = "";
        public $body = null;
        public $prevIntro = "";
        public $intro = "";

        public static function getInstance()
        {
            if ( is_null( self::$instance ) ) {
                self::$instance = new self;
            }

            return self::$instance;
        }

        public static function start( $intro )
        {
            $instance = self::getInstance();
            $instance->intro = $intro;
        }

        public static function add( $body )
        {
            $instance = self::getInstance();
            if( $instance->intro != $instance->prevIntro ) {
                $instance->prevIntro = $instance->intro;
                $instance->body .= '<strong>' . $instance->intro . '</strong><br />';
            }
            $instance->body = $instance->body . $body . '<br />';
        }

        public static function notify( $intro = null )
        {
            $instance = self::getInstance();
            $instance->subject = "MYERS ERROR REPORT " . current_time( 'mysql' );

            if( ! is_null( $instance->body ) ) {
                if( $intro != null ) {
                    $instance->body = $intro . "<br />" . $instance->body;
                }
                 
                wp_mail($instance->to, $instance->subject, $instance->body, $instance->headers);
            }
        }
    }

    EmailHandler::getInstance();
}