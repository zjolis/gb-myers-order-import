<?php

class ShippingHandler 
{
    public static $ship_id = [
        0 => [
            'id' => 24,
            'suffix' => 44
        ],
        2 => [
            'id' => 61,
            'suffix' => 67
        ],
        12 => [
            'id' => 54,
            'suffix' => 64
        ],
        15 => [
            'id' => 63,
            'suffix' => 74
        ],
        6 => [
            'id' => 27,
            'suffix' => 30
        ],
        3 => [
            'id' => 19,
            'suffix' => 25
        ],
        4 => [
            'id' => 20,
            'suffix' => 26
        ],
        17 => [
            'id' => 69,
            'suffix' => ''
        ],
        5 => [
            'id' => 25,
            'suffix' => 44
        ],
    ];

    public static function zoneShippingMethods($id) 
    {
        return self::$ship_id[$id];
    }

    public static function getShippingPackages()
    {
        $shipping = WC()->shipping;
        $shipping_packages = array();
        var_dump($shipping); exit;
        foreach ($shipping->get_packages() as $package_key => $package) {
            if (isset($package['rates'][$checkout->shipping_methods[$package_key]])) {
                $package = $package['rates'][$checkout->shipping_methods[$package_key]];
                $shipping_packages[$package_key] = array(
                    'id' => $package->id,
                    'label' => $package->label,
                    'cost' => $package->cost,
                    'taxes' => $package->taxes,
                    'method_id' => $package->method_id,
                    'meta_data' => $package->get_meta_data()
                );
            }
        }
        return $shipping_packages;
    }
  
    public static function getChoosenShippingMethod($billing)
    {
        // $postcode = $billing['postcode'];
        $postcode = 2454;
        // var_dump($postcode); exit;
        $zone_listing = [];
        $zone_for_postcode = null;
        $shipping_methods_for_postcode = null;
        foreach (WC_Shipping_Zones::get_zones() as $zone_id => $zone) {
            $postcode_list = [];
            $postcode_list = array_map(function($z) {
            if($z->type == "postcode") {
                return $z->code;
            }
            }, $zone['zone_locations']);
            if(in_array($postcode, $postcode_list)) {
                $zone_options = self::zoneShippingMethods($zone_id);
                $shipping_methods_for_postcode = $zone['shipping_methods'][$zone_options['id']];
            }
        }
  
        if(is_null($zone_for_postcode) && is_null($shipping_methods_for_postcode)) {
            $zone_options = self::zoneShippingMethods(0);
            $shipping_methods_for_postcode = WC_Shipping_Zones::get_zone(0)->get_shipping_methods();
            $shipping_methods_for_postcode = $shipping_methods_for_postcode[$zone_options['id']];
        }
        
        $choosen_shipping_method = $shipping_methods_for_postcode->get_rate_id($zone_options['suffix']);
        $shipping_rates = $shipping_methods_for_postcode->get_shipping_rates();
        $shipping_rates = array_filter($shipping_rates, function($rate) use ($zone_options) {
            return $rate->rate_id == $zone_options['suffix'];
        });
        return [
            "id" => $choosen_shipping_method,
            "label" => $shipping_rates[0]->rate_label,
            "label" => $shipping_rates[0]->rate_label,
        ];
    }
}