<?php

use League\Csv\Reader;
use League\Csv\Statement;

class CsvDataHandler 
{
    protected $startDetails = null;
    protected $endDetails = null;

    protected $orderNumberKey = "Order Number";
    protected $startDetailsKey = "Start Details";
    protected $endDetailsKey = "End Details";
    protected $notesKey = "Notes from Chain";
    protected $totalKey = "$ Value";


    protected $data;
    protected $order_items;
    protected $address;
    protected $meta_tags;

    protected $file;
    protected $csv;

    public function __construct($file)
    {
        $this->csv = Reader::createFromPath($file, 'r');
    }

    public function generateData()
    {
        $records = $this->csv->getRecords();
        foreach ($records as $offset => $record) {
            $key = $record[0];
            $value = $record[1];

            if($this->hasMatchedStartDetailsKey($key)) {
                $this->startDetails = $offset + 3;
            } else if ($this->hasMatchedEndDetailsKey($key)) {
                $this->endDetails = $offset - $this->startDetails;
            }

            $this->data[$key] = $value;
        }
    }

    public function generateOrderItems()
    {
        global $wpdb;
        $stmt = (new Statement())
            ->offset($this->startDetails)
            ->limit($this->endDetails);

        $records = $stmt->process($this->csv);


        foreach ($records as $offset => $record) {
          $barcode = trim($record[0], "'");
          $meta_key = 'myers_barcode';
          $meta_value = $barcode;
          $postids = $wpdb->get_results( 
            "
            SELECT post_id 
            FROM {$wpdb->postmeta}
            WHERE meta_key = '{$meta_key}' 
              AND meta_value = '{$meta_value}'
            ",
            OBJECT
          );
          $post_id = $postids[0]->post_id;

            $sku = trim($record[0], "'");
            $price = $record[1];
            $qty = $record[2];

            $product = wc_get_product($post_id);
            if($product) {
                $product_data = $product->get_data();
                $item = [
                    "id" => $product_data["id"],
                    "props" => [
                        "quantity" => $qty,
                        "variation" => [],
                        "subtotal" => $qty * $price,
                        "total" => $qty * $price,
                        "subtotal_tax" => 0,
                        "total_tax" => 0,
                        "taxes" => [ "total" => [], "subtotal" => []],
                        "name" => $product_data["name"],
                        "tax_class" => "",
                        "product_id" => $product_data["id"],
                        "variation_id" => 0,
                        "addons" => []
                    ]
                ];
            } else {
                $message = "BR404 : No product found using barcode {$barcode}";
                LogHandler::log($message);
                EmailHandler::add($message);
                $item = [
                    "id" => null,
                    "props" => [
                        "quantity" => $qty,
                        "variation" => [],
                        "subtotal" => $qty * $price,
                        "total" => $qty * $price,
                        "subtotal_tax" => 0,
                        "total_tax" => 0,
                        "taxes" => (object) [ "total" => [], "subtotal" => []],
                        "name" => "",
                        "tax_class" => "",
                        "product_id" => null,
                        "variation_id" => 0,
                        "addons" => []
                    ]
                ];
            }

            $this->order_items[] = $item;
        }
    }

    public function generateAddress()
    {
        $address = $this->data[$this->notesKey];
        $customer_data = explode("<EOL>", $address);

        $address = explode('+', $customer_data[4]);
        $phone = explode(":", $customer_data[7])[1];
        $phone = trim($phone, '?');
        // $email = explode(":", $customer_data[7])[1];
        $name = explode("::", $address[2]);

        $first_name = str_replace(":", "", $name[0]);
        $last_name = str_replace(":", "", $name[1]);

        $email = "sample@myers.com";

        $this->address["customer"]['id'] = $this->getCustomerId($email);
        $this->address["customer"]['name'] = $first_name . " " . $last_name;
        $this->address["customer"]['email'] = $email;
        $this->address["customer"]['phone'] = $phone;

        
        $this->address['shipping_address']["address_1"] = str_replace(":", "", $address[3]);
        $this->address['shipping_address']["address_2"] = '';
        $this->address['shipping_address']["city"] = str_replace(":", "", $address[4]);
        $this->address['shipping_address']["state"] = $address[5];
        $this->address['shipping_address']["postcode"] = $address[6];
        $this->address['shipping_address']["country"] = $address[7];
        $this->address['shipping_address']["first_name"] = $first_name;
        $this->address['shipping_address']["last_name"] = $last_name;
        $this->address['shipping_address']["company"] = '';
        
        $this->address['billing_address']["first_name"] = $first_name;
        $this->address['billing_address']["last_name"] = $last_name;
        $this->address["billing_address"]['email'] = $email;
        $this->address["billing_address"]['phone'] = $phone;

        if ($this->hasGreetingMessage($customer_data[11])) {
          $this->meta_tags["Gift Message"] = $this->getGreetingMessage($customer_data[11]);
        }

        $this->meta_tags['myers_order_number'] = $this->data[$this->orderNumberKey];

        return $this->address;
    }

    public function hasGreetingMessage($message)
    {
      return strpos($message, "Greeting Message") !== false;
    }

    public function getGreetingMessage($text)
    {
      // Remove `Greeting Message: `
      // Remove colons (:)
      return str_replace(
        ":",
        "",
        str_replace("Greeting Message: ", "", $text)
      );
    }

    public function hasMatchedStartDetailsKey($key)
    {
        return $key == $this->startDetailsKey;
    }
    public function hasMatchedEndDetailsKey($key)
    {
        return $key == $this->endDetailsKey;
    }

    public function getOrderData()
    {
        return [
            "gift_delivery_date" => $this->getDeliveryDate(),
            "cart_shipping_total" => $this->getShippingFee(),
            "cart_shipping_tax_total" => 0,
            "cart_discount_total" => $this->getOrderTwentyPercentDiscount(),
            "cart_discount_tax_total" => 0,
            "cart_tax_total" => 0,
            "cart_total" => $this->getOverAllTotal(),
            "cart_items" => $this->order_items,
            "cart_fees" => [],
            "cart_needs_shipping" => 0,
            "chosen_shipping_methods" => [],
            "shipping_packages" => [],
            "billing_address" => $this->address['billing_address'],
            "shipping_address" => $this->address['shipping_address'],
            "customer_id" => $this->address['customer']['id'],
        ];
    }

    public function getOrderMetas()
    {
      return $this->meta_tags;
    }

    public function getData()
    {
        return $this->data;
    }

    public function getBillingAddress()
    {
      return $this->address['shipping_address'];
    }

    public function getOrderItems()
    {
        return $this->order_items;
    }

    public function getOrderTotal()
    {
        return $this->data[$this->totalKey];
    }
    
    public function getShippingFee()
    {
        return 9.26;
    }

    public function getOverAllTotal()
    {
        return ($this->getOrderTotal() - $this->getOrderTwentyPercentDiscount()) + $this->getShippingFee();
    }

    public function getOrderTwentyPercentDiscount()
    {
        return $this->getOrderTotal() * 0.20;
    }

    public function getCustomerId($email)
    {
        $user = get_user_by( 'email', $email );
        
        if(!$user) {
            return false;
        }
        
        return $user->ID;
    }

    public function getDeliveryDate()
    {
      $date = "today";
      $date = strtotime($date);
      $date = strtotime("+3 day", $date);
      return date('F d, Y', $date);
    }
}