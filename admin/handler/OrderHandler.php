<?php

class OrderHandler
{
    public static function createOrder($order_initial_data, $order_meta_tags)
    {
        global $wpdb;

        try {
            // Start transaction if available
            wc_transaction_query('start');

            $checkout = WC()->checkout;

            $gift_delivery_date = $order_initial_data['gift_delivery_date'];
            $customer_id = $order_initial_data['customer_id'];
            // $cart_hash = get_post_meta( $post_id, 'cart_hash', true );
            $cart_shipping_total = (float) $order_initial_data['cart_shipping_total'];
            $cart_shipping_tax_total = (float)$order_initial_data['cart_shipping_tax_total'];
            $cart_discount_total = (float)$order_initial_data['cart_discount_total'];
            $cart_discount_tax_total = (float)$order_initial_data['cart_discount_tax_total'];
            $cart_tax_total = (float)$order_initial_data['cart_tax_total'];
            $cart_total = (float)$order_initial_data['cart_total'];
            $cart_items = $order_initial_data['cart_items'];
            $cart_fees = $order_initial_data['cart_fees'];
            $cart_coupons = $order_initial_data['cart_coupons'];
            $cart_taxes = $order_initial_data['cart_taxes'];
            $cart_needs_shipping = (bool)$order_initial_data['cart_needs_shipping'];
            if (version_compare(WC_VERSION, '3.0.0', '>=')) {
                $chosen_shipping_methods = $order_initial_data['chosen_shipping_methods'];
            }
            $shipping_packages = $order_initial_data['shipping_packages'];
            $billing_address = $order_initial_data['billing_address'];
            $shipping_address = $order_initial_data['shipping_address'];

            /**
             * @see WC_Checkout::create_order
             */

            $order = new WC_Order();

            $fields_prefix = [
                'shipping' => true,
                'billing' => true,
            ];

            $shipping_fields = [
                'shipping_method' => true,
                'shipping_total' => true,
                'shipping_tax' => true,
            ];

            // Create the WC_Order item.

            $order_data = [
                'status' => apply_filters('woocommerce_default_order_status', 'pending'),
                'customer_id' => $customer_id,
                'customer_note' => isset($order_initial_data['order_comments']) ? $order_initial_data['order_comments'] : '',
                'created_via' => 'checkout',
            ];

            $order = wc_create_order($order_data);

            if (is_wp_error($order)) {
                $message = 'OC520 : Unable to create order. Please try again.';
                LogHandler::log($message);
                EmailHandler::add($message);

                throw new Exception(sprintf(__('Error %d: Unable to create order. Please try again.', 'woocommerce'), 520));
            } elseif (false === $order) {
                $message = 'OC521 : Unable to create order. Please try again.';
                LogHandler::log($message);
                EmailHandler::add($message);

                throw new Exception(sprintf(__('Error %d: Unable to create order. Please try again.', 'woocommerce'), 521));
            } else {
                // avoid older WooCommerce error
                if (method_exists($order, 'get_id')) {
                    $order_id = $order->get_id();
                } else {
                    $order_id = $order->ID;
                }
                do_action('woocommerce_new_order', $order_id);
                do_action('woocommerce_process_shop_order_meta', $order_id, null);

                add_post_meta($order_id, 'Gift Delivery Date', $gift_delivery_date);
                add_post_meta($order_id, 'e_deliverydate', $gift_delivery_date);

                $order_number_formatted = get_post_meta($order_id, '_order_number_formatted', true);
                LogHandler::log("Order number: {$order_number_formatted}");
                $new_order_number = str_replace('GB', 'MY', $order_number_formatted);
                LogHandler::log("New Order number: {$new_order_number}");

                update_post_meta($order_id, '_order_number_formatted', $new_order_number);

                foreach ($order_meta_tags as $key => $value) {
                    add_post_meta($order_id, $key, $value);
                }

                $d = new \DateTime($gift_delivery_date);
                $d->settime(0, 0);
                $timestamp = $d->getTimestamp();
                add_post_meta($order_id, '_orddd_timestamp', $timestamp);
            }
            // Store the line items to the new/resumed order
            foreach ($cart_items as $cart_item_key => $cart_item) {
                if (version_compare(WC_VERSION, '3.0.0', '>=')) {
                    $values = [
                        'data' => wc_get_product($cart_item['id']),
                        'quantity' => $cart_item['props']['quantity'],
                        'variation' => $cart_item['props']['variation'],
                        'line_subtotal' => $cart_item['props']['subtotal'],
                        'line_total' => $cart_item['props']['total'],
                        'line_subtotal_tax' => $cart_item['props']['subtotal_tax'],
                        'line_tax' => $cart_item['props']['total_tax'],
                        'line_tax_data' => $cart_item['props']['taxes']
                    ];

                    $item = new WC_Order_Item_Product();
                    // $item->legacy_values        = $values; // @deprecated For legacy actions.
                    // $item->legacy_cart_item_key = $cart_item_key; // @deprecated For legacy actions.
                    $item->set_props([
                        'quantity' => $cart_item['props']['quantity'],
                        'variation' => $cart_item['props']['variation'],
                        'subtotal' => $cart_item['props']['subtotal'],
                        'total' => $cart_item['props']['total'],
                        'subtotal_tax' => $cart_item['props']['subtotal_tax'],
                        'total_tax' => $cart_item['props']['total_tax'],
                        // 'taxes'        => $cart_item['props']['taxes'],
                        'name' => $cart_item['props']['name'],
                        'tax_class' => $cart_item['props']['tax_class'],
                        'product_id' => $cart_item['props']['product_id']
                    ]);

                    // FOR ADDONS
                    // if ( ! empty( $cart_item['props']['addons'] ) ) {
                    //   foreach ( $cart_item['props']['addons'] as $addon ) {
                    //     $name = $addon['name'];
                    //     if ( $addon['price'] > 0 && apply_filters( 'woocommerce_addons_add_price_to_name', true ) ) {
                    //                       $name .= ' (' . strip_tags( woocommerce_price( get_product_addon_price_for_display ( $addon['price'], $item->get_data(), true ) ) ) . ')';
                    //                 }
                    //     $item->add_meta_data($name, $addon['value']);
                    //   }
                    // }

                    $item->set_backorder_meta();

                    do_action('woocommerce_checkout_create_order_line_item', $item, $cart_item_key, $values, $order);

                    // Add item to order and save.
                    $order->add_item($item);
                } else {
                    $product = new $cart_item['class']($cart_item['id']);
                    unset($cart_item['class'], $cart_item['id']);

                    $cart_item['data'] = $product;

                    $item_id = $order->add_product(
                        $product,
                        $cart_item['quantity'],
                        [
                            'variation' => $cart_item['variation'],
                            'totals' => $cart_item['totals']
                        ]
                    );

                    // if ( ! empty( $cart_item['props']['addons'] ) ) {
                    // 	foreach ( $cart_item['props']['addons'] as $addon ) {
                    // 		$name = $addon['name'];
                    // 		if ( $addon['price'] > 0 && apply_filters( 'woocommerce_addons_add_price_to_name', true ) ) {
                    // 			$name .= ' (' . strip_tags( woocommerce_price( get_product_addon_price_for_display ( $addon['price'], $item_id->get_data(), true ) ) ) . ')';
                    // 		}
                    // 		$item_id->add_meta_data($name, $addon['value']);
                    // 	}
                    // }

                    if (!$item_id) {
                        $message = 'OC525 : Unable to create product for ' . $new_order_number;
                        LogHandler::log($message);
                        EmailHandler::add($message);
                        throw new Exception(sprintf(__('Error %d: Unable to create order. Please try again.', 'woocommerce'), 525));
                    }

                    // Allow plugins to add order item meta
                    do_action('woocommerce_add_order_item_meta', $item_id, $cart_item, $cart_item_key);
                }
            }

            // Store fees
            foreach ($cart_fees as $fee_key => $fee) {
                // $fee needs to be an object, so we parsed the JSON to an object,
                // but $fee->tax_data needs to be an associative array, with numeric keys.
                // Just convert it now.
                //$fee->tax_data = (array)$fee->tax_data; # This keeps the array keys as strings. We want integers.
                $tax_data = [];
                foreach ($fee->tax_data as $key_str => $amount) {
                    $tax_data[(int)$key_str] = $amount;
                }
                $fee->tax_data = $tax_data;

                $item_id = $order->add_fee($fee);

                if (!$item_id) {
                    $message = 'OC526 : Unable to add fee to  order ' . $new_order_number;
                    LogHandler::log($message);
                    EmailHandler::add($message);
                    throw new Exception(sprintf(__('Error %d: Unable to create order. Please try again.', 'woocommerce'), 526));
                }

                // Allow plugins to add order item meta to fees
                do_action('woocommerce_add_order_fee_meta', $order_id, $item_id, $fee, $fee_key);
            }

            // Store shipping for all packages
            // foreach ( $shipping_packages as $package_key => $package_data ) {
            // $package = new WC_Shipping_Rate( $package_data['id'], $package_data['label'], $package_data['cost'], $package_data['taxes'], $package_data['method_id'] );

            if (version_compare(WC_VERSION, '3.0.0', '>=')) {
                // if (in_array($package->id, $chosen_shipping_methods)) {
                $item = new WC_Order_Item_Shipping;
                // $item->legacy_package_key = $package_key; // @deprecated For legacy actions.
                $item->set_props([
                    'method_title' => 'Myer Shipping',
                    // 'method_id'    => $package->id,
                    'total' => wc_format_decimal($cart_shipping_total),
                    // 'taxes'        => array(
                    //   'total' => $package->taxes,
                    // ),
                ]);

                // foreach ( $package_data['meta_data'] as $key => $value ) {
                //   $item->add_meta_data( $key, $value, true );
                // }

                /**
                 * Action hook to adjust item before save.
                 * @since 3.0.0
                 */
                do_action('woocommerce_checkout_create_order_shipping_item', $item, $package_key, $package, $order);

                // Add item to order and save.
                $order->add_item($item);
            // }
            } else {
                // foreach ($package_data['meta_data'] as $key => $value) {
            //   $package->add_meta_data($key, $value);
            // }

            // $item_id = $order->add_shipping( $package );

            // if ( ! $item_id ) {
            //   throw new Exception( sprintf( __( 'Error %d: Unable to create order. Please try again.', 'woocommerce' ), 527 ) );
            // }

            // // Allows plugins to add order item meta to shipping
            // do_action( 'woocommerce_add_shipping_order_item', $order_id, $item_id, $package_key );
            }
            // }

            // Store tax rows
            foreach ($cart_taxes as $tax_rate_id => $cart_tax) {
                if (!$order->add_tax($tax_rate_id, $cart_tax['tax_amount'], $cart_tax['shipping_tax_amount'])) {
                    $message = 'OC528 : Unable to add tax to  order ' . $new_order_number;
                    LogHandler::log($message);
                    EmailHandler::add($message);
                    throw new Exception(sprintf(__('Error %d: Unable to create order. Please try again.', 'woocommerce'), 528));
                }
            }

            // Store coupons
            foreach ($cart_coupons as $code => $coupon_data) {
                if (!$order->add_coupon($code, $coupon_data['discount_amount'], $coupon_data['discount_tax_amount'])) {
                    $message = 'OC529 : Unable to add coupon to  order ' . $new_order_number;
                    LogHandler::log($message);
                    EmailHandler::add($message);
                    throw new Exception(sprintf(__('Error %d: Unable to create order. Please try again.', 'woocommerce'), 529));
                }
            }

            $order->set_address($billing_address, 'billing');
            $order->set_address($shipping_address, 'shipping');
            if (version_compare(WC_VERSION, '3.0.0', '>=')) {
                $order->set_prices_include_tax('yes' === get_option('woocommerce_prices_include_tax'));
                $order->set_customer_ip_address(WC_Geolocation::get_ip_address());
                $order->set_customer_user_agent(wc_get_user_agent());
            }
            $order->set_payment_method('On Account');
            if (version_compare(WC_VERSION, '3.0.0', '>=')) {
                $order->set_shipping_total($cart_shipping_total);
            } else {
                $order->set_total($cart_shipping_total, 'shipping');
            }
            $order->set_total($cart_discount_total, 'cart_discount');
            $order->set_total($cart_discount_tax_total, 'cart_discount_tax');
            $order->set_total($cart_tax_total, 'tax');
            if (version_compare(WC_VERSION, '3.0.0', '>=')) {
                $order->set_shipping_tax($cart_shipping_tax_total);
            } else {
                $order->set_total($cart_shipping_tax_total, 'shipping_tax');
            }
            $order->set_total($cart_total);
            $order->payment_complete();
            $order->update_status('processing');

            // Update user meta
            if ($customer_id) {
                // Can't do the following unless we can get an instance of the WC_Checkout...
          /*if ( apply_filters( 'woocommerce_checkout_update_customer_data', true, $checkout ) ) {
            foreach ( $billing_address as $key => $value ) {
              update_user_meta( $customer_id, 'billing_' . $key, $value );
            }
            if ( WC()->cart->needs_shipping() ) {
              foreach ( $shipping_address as $key => $value ) {
                update_user_meta( $customer_id, 'shipping_' . $key, $value );
              }
            }
          }
          do_action( 'woocommerce_checkout_update_user_meta', $customer_id, $posted );*/
            }

            // Let plugins add meta
            // do_action( 'woocommerce_checkout_update_order_meta', $order_id, $posted );

            // If we got here, the order was created without problems!
            wc_transaction_query('commit');
        } catch (Exception $e) {
            // There was an error adding order data!
            $message = "OC500 : An error occured when creating order\n" . $e->getMessage();
            LogHandler::log($message);
            EmailHandler::add($message);

            wc_transaction_query('rollback');
            return new WP_Error('checkout-error', $e->getMessage());
        }

        return $order;
    }

    public static function getCustomerId($email)
    {
        $user = get_user_by('email', $email);

        if (!$user) {
            return false;
        }

        return $user->ID;
    }
}
