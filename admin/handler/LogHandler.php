<?php

if ( ! class_exists('LogHandler') ) {
    class LogHandler
    {
        private static $instance;

        /**
         * Public static variables.
         *
         * @var		WC_Logger|null		$log			An instance of the WC_Logger class. Defaults to null.
         */
        private static $log;

        public $target = 'gb_myers';

        public static function getInstance()
        {
            if ( is_null( self::$instance ) ) {
                self::$instance = $instance;
            }

            return self::$instance;
        }

        /**
         * Logging method. Using this to log a string will store it in a file that is accessible
         * from "WooCommerce > System Status > Logs" in the WordPress admin. No FTP access required.
         *
         * @param 	string	$message	The message to log.
         * @uses	WC_Logger::add()
         */
        public static function log( $message ) 
        {
            $instance = self::getInstance();
            
            if ( is_null( self::$log ) ) {
                self::$log = new WC_Logger;
            }
            
            if (is_array($message)) {
                /**
                 * @since 2.1.0
                 * Properly expand Arrays in logs.
                 */
                $message = print_r($message, true);
            } elseif(is_object($message)) {
                /**
                 * @since 2.1.0
                 * Properly expand Objects in logs.
                 * 
                 * Only use the Output Buffer if it's not currently active,
                 * or if it's empty.
                 *
                 * Note:	If the Output Buffer is active but empty, we write to it,
                 * 			read from it, then discard the contents while leaving it active.
                 *
                 * Otherwise, if $message is an Object, it will be logged as, for example:
                 * (foo Object)
                 */
                $ob_get_length = ob_get_length();
                if (!$ob_get_length) {
                    if ($ob_get_length === false) {
                        ob_start();
                    }
                    var_dump($message);
                    $message = ob_get_contents();
                    if ($ob_get_length === false) {
                        ob_end_clean();
                    } else {
                        ob_clean();
                    }
                } else {
                    $message = '(' . get_class($message) . ' Object)';
                }
            }
            self::$log->add( $instance->target, $message );
        }
    }

    LogHandler::getInstance();
}