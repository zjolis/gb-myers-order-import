<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://blaze.online
 * @since      1.0.0
 *
 * @package    Gb_Myers_Order_Import
 * @subpackage Gb_Myers_Order_Import/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Gb_Myers_Order_Import
 * @subpackage Gb_Myers_Order_Import/admin
 * @author     Blaze Online <support@blaze.online>
 */
class Gb_Myers_Order_Import_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Gb_Myers_Order_Import_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Gb_Myers_Order_Import_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/gb-myers-order-import-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Gb_Myers_Order_Import_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Gb_Myers_Order_Import_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/gb-myers-order-import-admin.js', array( 'jquery' ), $this->version, false );

  }

  public function myers_barcode_custom_field()
  {
    $args = array(
      'id' => 'myers_barcode',
      'label' => __( 'Myers Barcode', 'blaze' ),
      'class' => 'blaze-custom-field',
      'desc_tip' => true,
      'description' => __( 'Enter the barcode from myers to map the products correctly on import.', 'blaze' ),
      );
      woocommerce_wp_text_input( $args );
  }

  public function myers_barcode_save( $post_id )
  {
    $product = wc_get_product( $post_id );
    $barcode = isset( $_POST['myers_barcode'] ) ? $_POST['myers_barcode'] : '';
    $product->update_meta_data( 'myers_barcode', sanitize_text_field( $barcode ) );
    $product->save();
  }
}
