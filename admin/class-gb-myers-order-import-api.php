<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://blaze.online
 * @since      1.0.0
 *
 * @package    Gb_Myers_Order_Import
 * @subpackage Gb_Myers_Order_Import/api
 */

require_once plugin_dir_path( __FILE__ ) . 'handler/EmailHandler.php';
require_once plugin_dir_path( __FILE__ ) . 'handler/LogHandler.php';
require_once plugin_dir_path( __FILE__ ) . 'handler/CsvDataHandler.php';
require_once plugin_dir_path( __FILE__ ) . 'handler/OrderHandler.php';
require_once plugin_dir_path( __FILE__ ) . 'handler/ShippingHandler.php';

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Gb_Myers_Order_Import
 * @subpackage Gb_Myers_Order_Import/api
 * @author     Blaze Online <support@blaze.online>
 */
class Gb_Myers_Order_Import_Api {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
    private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

    }
  
  public function register_endpoints()
  {
    register_rest_route( 'gb_myers/v1', '/import_order_manual', array(
      'methods' => 'POST',
      'callback' => array($this, 'import_order_manual'),
    ) );
    register_rest_route( 'gb_myers/v1', '/import_order', array(
      'methods' => 'GET',
      'callback' => array($this, 'order_import_via_cron_2'),
    ) );
  }

  public function import_order( WP_REST_Request $request ) 
  {
    return 'nani!';
  }

  public function order_import_via_cron_2( WP_REST_Request $request ) 
  {
    // var_dump($request['folder']); exit;
    $directory = GB_MYERS_UPLOAD_DIR . $request['folder'];
    $move_path = GB_MYERS_UPLOAD_FINISHED_DIR . $request['folder'];

    LogHandler::log("Request Folder: " . $request['folder']);


    $scanned_directory = glob($directory . "/*.csv");
    LogHandler::log("Scanned directories:");
    LogHandler::log($scanned_directory);
    
    if(count($scanned_directory) > 0) {
      foreach ($scanned_directory as $file) {
        $data = [
          'filepath' => $file,
          'folder' => $request['folder']
        ];
        EmailHandler::start("Folder: " . $request['folder'] ."; File: " . $file);
        
        LogHandler::log("Request array:");
        LogHandler::log($data);
        $import = $this->import_order_manual($data);

        LogHandler::log("Import result:");
        LogHandler::log($import);
      }

      EmailHandler::notify();

      rename($directory,  $move_path);

      return "Imported successfully!";
    }

    return $scanned_directory;
  }

  public function order_import_init() 
  {
    if ( ! wp_next_scheduled( 'order_import_via_cron_2' ) ) {
      wp_schedule_event( time(), 'five_minutes', 'order_import_via_cron_2' );
    }
  }

  public function add_five_seconds_interval( $schedules ) 
  {
    $schedules['five_seconds'] = array(
        'interval' => 5,
        'display'  => esc_html__( 'Every Five Seconds' ),
    );

    return $schedules;
  }
  
  /**
  * 
  *
  * @param array 
  * @return string|null 
  */
// public function import_order_manual( WP_REST_Request $request ) 
public function import_order_manual( $request ) 
{  
    if(!file_exists($request['filepath'])) {
      return [
        "error" => 1,
        "message" => "filename not found"
      ];
    }

    $csv = new CsvDataHandler($request['filepath']);

    $csv->generateData();
    $csv->generateOrderItems();
    $csv->generateAddress();

    // ShippingHandler::getShippingPackages();
    // var_dump(ShippingHandler::getChoosenShippingMethod($csv->getBillingAddress())); exit;

    // exit;
    if($order = OrderHandler::createOrder( $csv->getOrderData(), $csv->getOrderMetas() )) {
        return [
            "success" => 1,
            "message" => "Myers Order Imported successfully.",
            "data" => $csv->getOrderData(),
            "order" => $order->get_data()
        ];
    }
}

// Function to remove folders and files 
public function rrmdir($dir) {
  if (is_dir($dir)) {
      $files = scandir($dir);
      foreach ($files as $file)
          if ($file != "." && $file != "..") rrmdir("$dir/$file");
      rmdir($dir);
  }
  else if (file_exists($dir)) unlink($dir);
}

// Function to Copy folders and files       
public function rcopy($src, $dst) {
  if (file_exists ( $dst ))
      rrmdir ( $dst );
  if (is_dir ( $src )) {
      mkdir ( $dst );
      $files = scandir ( $src );
      foreach ( $files as $file )
          if ($file != "." && $file != "..")
              rcopy ( "$src/$file", "$dst/$file" );
  } else if (file_exists ( $src ))
      copy ( $src, $dst );
}

    /**
     * myers_update_meta_after_order_creation
     * 
     * Updates meta tags after order creation.
     * Here we update the Order Title from GBxxxxx to MYxxxxx
     *
     * @param [int] $order_id
     * @return void
     */
    public function myers_update_meta_after_order_creation( $order_id )
    {
        $myers_order_number = get_post_meta($order_id, 'myers_order_number', true);
        if($myers_order_number) {
            $order_number_formatted = get_post_meta($order_id, "_order_number_formatted", true);
            update_post_meta($order_id, "_order_number_formatted", str_replace("GB", "MY", $order_number_formatted));
        }
    }
}
