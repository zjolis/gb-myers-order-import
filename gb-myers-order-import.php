<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://blaze.online
 * @since             1.0.0
 * @package           Gb_Myers_Order_Import
 *
 * @wordpress-plugin
 * Plugin Name:       GB Myers Order Import
 * Plugin URI:        http://blaze.online
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            Blaze Online
 * Author URI:        http://blaze.online
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       gb-myers-order-import
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'PLUGIN_NAME_VERSION_2', '1.0.0' );

/**
 * Upload Directory (Source)
 * This is where the cron checks for new file. If there are new files, then add it to order creation
 */
define( 'GB_MYERS_UPLOAD_DIR', WP_CONTENT_DIR . '/uploads/gb_myers/new/' );
define( 'GB_MYERS_UPLOAD_FINISHED_DIR', WP_CONTENT_DIR . '/uploads/gb_myers/finished/' );

/**
 * Log directory
 * This is where action logs can be found. This is for debugging purposes.
 */
define( 'GB_MYERS_LOG_DIR', WP_CONTENT_DIR . '/logs/gb_myers/' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-gb-myers-order-import-activator.php
 */
function activate_gb_myers_order_import() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-gb-myers-order-import-activator.php';
	Gb_Myers_Order_Import_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-gb-myers-order-import-deactivator.php
 */
function deactivate_gb_myers_order_import() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-gb-myers-order-import-deactivator.php';
	Gb_Myers_Order_Import_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_gb_myers_order_import' );
register_deactivation_hook( __FILE__, 'deactivate_gb_myers_order_import' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-gb-myers-order-import.php';

/**
 * Composer dependencies
 * This loads the following:
 * 1. CSV PHP
 */
require plugin_dir_path( __FILE__ ) . 'vendor/autoload.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_gb_myers_order_import() {

	$plugin = new Gb_Myers_Order_Import();
	$plugin->run();

}
run_gb_myers_order_import();
