<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://blaze.online
 * @since      1.0.0
 *
 * @package    Gb_Myers_Order_Import
 * @subpackage Gb_Myers_Order_Import/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Gb_Myers_Order_Import
 * @subpackage Gb_Myers_Order_Import/includes
 * @author     Blaze Online <support@blaze.online>
 */
class Gb_Myers_Order_Import_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {
    $timestamp = wp_next_scheduled( 'order_import_via_cron_2' );
    wp_unschedule_event( $timestamp, 'order_import_via_cron_2' );
	}

}
