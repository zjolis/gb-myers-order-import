<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       http://blaze.online
 * @since      1.0.0
 *
 * @package    Gb_Myers_Order_Import
 * @subpackage Gb_Myers_Order_Import/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Gb_Myers_Order_Import
 * @subpackage Gb_Myers_Order_Import/includes
 * @author     Blaze Online <support@blaze.online>
 */
class Gb_Myers_Order_Import {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Gb_Myers_Order_Import_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
		if ( defined( 'PLUGIN_NAME_VERSION_2' ) ) {
			$this->version = PLUGIN_NAME_VERSION_2;
		} else {
			$this->version = '1.0.0';
		}
		$this->plugin_name = 'gb-myers-order-import';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		// $this->define_public_hooks();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Gb_Myers_Order_Import_Loader. Orchestrates the hooks of the plugin.
	 * - Gb_Myers_Order_Import_i18n. Defines internationalization functionality.
	 * - Gb_Myers_Order_Import_Admin. Defines all hooks for the admin area.
	 * - Gb_Myers_Order_Import_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-gb-myers-order-import-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-gb-myers-order-import-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
    require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-gb-myers-order-import-admin.php';
    
    /**
		 * The class responsible for defining all actions that occur in the api area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-gb-myers-order-import-api.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		// require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-gb-myers-order-import-public.php';

		$this->loader = new Gb_Myers_Order_Import_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Gb_Myers_Order_Import_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Gb_Myers_Order_Import_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new Gb_Myers_Order_Import_Admin( $this->get_plugin_name(), $this->get_version() );
    $plugin_api = new Gb_Myers_Order_Import_Api( $this->get_plugin_name(), $this->get_version() );
    
    // // Filters
		// add_filter( 'cron_schedules', function ( $schedules ) {
    //   $schedules['five_minutes'] = array(
    //       'interval' => 5 * 60,
    //       'display'  => esc_html__( 'Every Five Seconds' ),
    //   );
  
    //   return $schedules;
    // });

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );
		$this->loader->add_action( 'woocommerce_product_options_general_product_data', $plugin_admin, 'myers_barcode_custom_field' );
		$this->loader->add_action( 'woocommerce_process_product_meta', $plugin_admin, 'myers_barcode_save' );
    // $this->loader->add_action( 'order_import_via_cron_2', $plugin_api, 'order_import_via_cron_2' );

    // if ( ! wp_next_scheduled( 'order_import_via_cron_2' ) ) {
    //   wp_schedule_event( time(), 'five_minutes', 'order_import_via_cron_2' );
    // }
    // $this->loader->add_action( 'wp', $plugin_api, 'order_import_init' );
    $this->loader->add_action( 'rest_api_init', $plugin_api, 'register_endpoints' );
    // $this->loader->add_action( 'woocommerce_checkout_update_order_meta', $plugin_api, 'myers_update_meta_after_order_creation' );
		// $this->loader->add_action( 'admin_init', $plugin_api, 'order_import_via_cron_2' );

  }

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new Gb_Myers_Order_Import_Public( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );

	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Gb_Myers_Order_Import_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

}
