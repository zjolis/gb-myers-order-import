<?php

/**
 * Fired during plugin activation
 *
 * @link       http://blaze.online
 * @since      1.0.0
 *
 * @package    Gb_Myers_Order_Import
 * @subpackage Gb_Myers_Order_Import/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Gb_Myers_Order_Import
 * @subpackage Gb_Myers_Order_Import/includes
 * @author     Blaze Online <support@blaze.online>
 */
class Gb_Myers_Order_Import_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {
    // self::createUploadsDirectory();
  }
  
  public function createUploadsDirectory()
  {
    if (!file_exists(GB_MYERS_UPLOAD_DIR)) {
      mkdir(GB_MYERS_UPLOAD_DIR, 0777, true);
    }
    if (!file_exists(GB_MYERS_UPLOAD_FINISHED_DIR)) {
      mkdir(GB_MYERS_UPLOAD_FINISHED_DIR, 0777, true);
    }
  }

}
